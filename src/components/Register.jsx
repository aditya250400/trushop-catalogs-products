import { Input } from "antd";
import { useFormik } from "formik";
import { useNavigate, Link } from "react-router-dom";
import * as Yup from "yup";
import axios from "axios";
import Modal from "react-modal";
import { useState } from "react";

const rulesSchema = Yup.object({
  email: Yup.string().required("Email harus di isi").email("Email tidak valid"),
  username: Yup.string().required("Username harus di isi"),
  name: Yup.string().required("Nama harus di isi"),
  password: Yup.string().required("Password harus di isi"),
  password_confirmation: Yup.string().required(
    "Konfirmasi Password harus di isi"
  ),
});

const Register = () => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const initialInputData = {
    email: "",
    username: "",
    name: "",
    password: "",
    password_confirmation: "",
  };

  const navigate = useNavigate();

  const closeModal = () => {
    setModalIsOpen(false);
    navigate("/login");
  };

  const handleOnSubmit = async (values) => {
    try {
      // eslint-disable-next-line
      let response = await axios.post(
        "https://api-project.amandemy.co.id/api/register",
        values
      );
      setModalIsOpen(true);
    } catch (error) {
      alert(error.response.data.info);
    }
  };

  const { handleChange, values, handleSubmit, errors, touched } = useFormik({
    initialValues: initialInputData,
    onSubmit: handleOnSubmit,
    validationSchema: rulesSchema,
  });

  return (
    <>
      <div className="bg-white mt-14 rounded-lg py-16 px-12 max-w-xl md:max-w-2xl md:mx-auto mx-10 shadow-lg">
        <h1 className="text-center text-lg md:text-2xl font-bold">
          Register Form
        </h1>
        <div>
          <h1 className="md:text-lg text-sm">Email</h1>
          <Input
            placeholder="Masukan Email"
            style={{ padding: "7px", marginTop: "6px" }}
            name="email"
            onChange={handleChange}
            value={values.email}
            className={
              touched.email === true && errors.email
                ? "border border-red-500"
                : ""
            }
          />
          <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
            {touched.email === true && errors.email}
          </p>
        </div>
        <div className="my-4">
          <h1 className="md:text-lg text-sm">Username</h1>
          <Input
            placeholder="Masukan Username"
            style={{ padding: "7px", marginTop: "6px" }}
            name="username"
            value={values.username}
            onChange={handleChange}
            className={
              touched.username === true && errors.username
                ? "border border-red-500"
                : ""
            }
          />
          <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
            {touched.username === true && errors.username}
          </p>
        </div>
        <div className="my-4">
          <h1 className="md:text-lg text-sm">Nama</h1>
          <Input
            placeholder="Masukan Nama"
            style={{ padding: "7px", marginTop: "6px" }}
            name="name"
            value={values.name}
            onChange={handleChange}
            className={
              touched.name === true && errors.name
                ? "border border-red-500"
                : ""
            }
          />
          <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
            {touched.name === true && errors.name}
          </p>
        </div>
        <div className="my-4">
          <h1 className="md:text-lg text-sm">Password</h1>
          <Input.Password
            placeholder="Masukan Password"
            style={{ padding: "7px", marginTop: "6px" }}
            name="password"
            value={values.password}
            onChange={handleChange}
            className={
              touched.password === true && errors.password
                ? "border border-red-500"
                : ""
            }
          />
          <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
            {touched.password === true && errors.password}
          </p>
        </div>
        <div className="my-4">
          <h1 className="md:text-lg text-sm">Konfirm Password</h1>
          <Input.Password
            placeholder="Masukan Ulang Password"
            style={{ padding: "7px", marginTop: "6px" }}
            name="password_confirmation"
            value={values.password_confirmation}
            onChange={handleChange}
            className={
              touched.password_confirmation === true &&
              errors.password_confirmation
                ? "border border-red-500"
                : ""
            }
          />
          <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
            {touched.password_confirmation === true &&
              errors.password_confirmation}
          </p>
        </div>

        <div className="flex justify-between mt-4">
          <div>
            <h1 className="text-xs md:text-lg">Sudah Punya akun?</h1>
            <Link to="/login" className="inline-block">
              <button className="text-blue-400 hover:text-white hover:text-slate-600 text-xs md:text-lg">
                Login
              </button>
            </Link>
          </div>
          <div>
            <button
              type="button"
              className="btn btn-success btn-sm p-1 md:btn-md capitalize text-white"
              onClick={handleSubmit}
            >
              Register
            </button>
          </div>
        </div>
      </div>
      <Modal
        appElement={document.getElementById("root")}
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        className="rounded-lg mx-10 mt-28 h-4/5  "
        style={{
          overlay: {
            backgroundColor: "rgba(0, 0, 0, 0.5)",
          },
        }}
      >
        <div className="text-center md:mx-40 bg-white p-10 rounded-lg">
          <h1 className="text-2xl block my-5">
            Berhasil Register. Anda akan di arahkan ke halaman Login
          </h1>
          <div>
            <button
              className="btn btn-info text-white"
              onClick={() => closeModal()}
            >
              Oke
            </button>
          </div>
        </div>
      </Modal>
    </>
  );
};
export default Register;
