import axios from "axios";
import { Link } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";
import Modal from "react-modal";

const TableProducts = () => {
  const {
    products,
    myFetchData,
    statusLoading,
    modalIsOpen,
    setModalIsOpen,
    statusLogin,
    setStatusLogin,
  } = useContext(GlobalContext);
  const [idProduct, setIdProduct] = useState(null);
  const [statusDelete, setStatusDelete] = useState(false);
  const [filterProducts, setFilterProduct] = useState({
    search: "",
    category: "",
  });
  const [backupProduct, setBackupProduct] = useState([]);
  const handleChange = (e) => {
    setFilterProduct({ ...filterProducts, [e.target.name]: e.target.value });
  };

  const onSearch = () => {
    let tmpProducts = structuredClone(products);

    if (filterProducts.search !== "") {
      tmpProducts = tmpProducts.filter((item) => {
        return item.name
          .toLowerCase()
          .includes(filterProducts.search.toLowerCase());
      });
    }
    if (filterProducts.category !== "") {
      tmpProducts = tmpProducts.filter((item) => {
        return item.category
          .toLowerCase()
          .includes(filterProducts.category.toLowerCase());
      });
    }
    setBackupProduct(tmpProducts);
  };

  const onReset = () => {
    setFilterProduct({
      search: "",
      category: "",
    });
    setBackupProduct(products);
  };

  const openModal = (id) => {
    setModalIsOpen(true);
    setIdProduct(id);
  };
  const closeModal = () => {
    setModalIsOpen(false);
    setStatusLogin(false);
    setStatusDelete(false);
  };

  const onDelete = async () => {
    try {
      // eslint-disable-next-line
      let response = await axios.delete(
        `https://api-project.amandemy.co.id/api/final/products/${idProduct}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setStatusDelete(true);
      setModalIsOpen(true);
      setIdProduct(null);
      myFetchData();
    } catch (error) {
      console.log(error.response.data.message);
      alert(error.response.data.message);
    }
  };

  useEffect(() => {
    setBackupProduct(products);
    // eslint-disable-next-line
  }, [products]);

  useEffect(() => {
    myFetchData();
    // eslint-disable-next-line
  }, []);

  return (
    <div className="mx-2 md:mx-8 my-8">
      <h1 className="text-center text-3xl text-cyan-400">Table Products</h1>
      {statusLoading === true ? (
        <div className="flex justify-center my-40">
          <span className="loading loading-spinner loading-lg text-cyan-500"></span>
        </div>
      ) : (
        <>
          <div className="flex justify-end my-2 py-2 px-3  ">
            <div>
              <Link
                to="/create"
                className="p-2 bg-white border border-cyan-500 text-cyan-500 rounded-lg text-xs md:text-lg btn-info hover:text-white font-semibold"
              >
                Create Product +
              </Link>
            </div>
          </div>
          <div className="flex justify-end my-2 py-2 md:px-3">
            <div className="mr-2">
              <select
                name="category"
                className=" py-1 px-1 md:px-2 rounded-md bg-white border border-gray-400 text-xs md:text-sm"
                onChange={handleChange}
                value={filterProducts.category}
              >
                <option value="" disabled>
                  Filter Kategori
                </option>
                <option value="teknologi">Teknologi</option>
                <option value="makanan">Makanan</option>
                <option value="minuman">Minuman</option>
                <option value="hiburan">Hiburan</option>
                <option value="kendaraan">Kendaraan</option>
              </select>
            </div>

            <div className="mr-2">
              <input
                type="text"
                className="block border border-gray-400 rounded-md w-24 md:w-32 px-1 md:px-2 py-1 text-xs md:text-sm"
                placeholder="Search"
                name="search"
                onChange={handleChange}
                value={filterProducts.search}
              />
            </div>
            <div className="mr-2">
              <button
                type="button"
                className="btn btn-info text-white btn-xs md:btn-sm text-xs md:text-sm mr-2 capitalize"
                onClick={onSearch}
              >
                Search
              </button>
              <button
                type="button"
                onClick={onReset}
                className="btn btn-error btn-xs text-xs md:btn-sm md:text-sm text-white capitalize"
              >
                Reset
              </button>
            </div>
          </div>

          <div className="overflow-x-auto">
            <table className=" table text-center bg-white">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Id Barang</th>
                  <th>Nama Barang</th>
                  <th>Harga Barang</th>
                  <th>Stock Barang</th>
                  <th>Gambar Barang</th>
                  <th>Deskripsi Barang</th>
                  <th>Status Diskon</th>
                  <th>Harga Diskon</th>
                  <th>Kategori Barang</th>
                  <th>Dibuat Oleh</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                {backupProduct.length > 0 ? (
                  backupProduct.map((product, index) => {
                    return (
                      <tr key={product.id}>
                        <td>{index + 1}</td>
                        <td>{product.id}</td>
                        <td>
                          <div className="h-32 overflow-auto ">
                            {product.name}
                          </div>
                        </td>
                        <td>{product.harga}</td>
                        <td>{product.stock}</td>
                        <td>
                          <a href={product.image_url} target="blank">
                            <img src={product.image_url} alt={product.nama} />
                          </a>
                        </td>
                        <td>
                          <div className="h-32 overflow-auto ">
                            {product.description}
                          </div>
                        </td>
                        <td>{product.is_diskon ? "Aktif" : "Tidak Aktif"}</td>
                        <td>
                          {product.harga_diskon === 0
                            ? "-"
                            : product.harga_diskon}
                        </td>
                        <td>{product.category}</td>
                        <td>{product.user.username}</td>
                        <td>
                          <div className="flex gap-2">
                            <Link to={`/update/${product.id}`}>
                              <button className="text-white btn btn-xs btn-success capitalize">
                                Update
                              </button>
                            </Link>
                            <button
                              className="text-white btn-xs btn btn-error capitalize"
                              onClick={() => openModal(product.id)}
                            >
                              Delete
                            </button>
                          </div>
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <tr>
                    <td colSpan="12">
                      <div className="my-24">
                        <h1 className="text-3xl">Tidak Ada Produk</h1>
                      </div>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </>
      )}
      <Modal
        appElement={document.getElementById("root")}
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        className="rounded-lg mx-8 mt-28 h-4/5  "
        style={{
          overlay: {
            backgroundColor: "rgba(0, 0, 0, 0.5)",
          },
        }}
      >
        <div className="text-center md:mx-40 bg-white p-10 rounded-lg">
          <h1 className="text-lg block my-5">
            {statusLogin === true
              ? "Berhasil Login"
              : statusDelete === true
              ? "Berhasil hapus produk"
              : "Apakah Anda yakin?"}
          </h1>
          <div>
            {statusLogin === false && statusDelete === false && (
              <>
                <button
                  className="btn btn-error text-white mx-2"
                  onClick={onDelete}
                >
                  Hapus
                </button>
              </>
            )}
            <button
              className="btn btn-info text-white"
              onClick={() => closeModal()}
            >
              {statusLogin === true || statusDelete === true ? "Oke" : "Batal"}
            </button>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default TableProducts;
