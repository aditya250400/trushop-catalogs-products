import { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";
import { useParams, useNavigate, Link } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import Modal from "react-modal";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama Barang harus di isiii"),
  harga: Yup.number()
    .typeError("Harga Barang harus berupa angka")
    .required("Harga Barang harus di isi"),
  stock: Yup.number()
    .typeError("Stock Barang harus berupa angka")
    .required("Stock Barang harus di isi"),
  image_url: Yup.string()
    .url("Url gambar tidak valid")
    .required("Url Gambar Barang harus di isi"),
  harga_diskon: Yup.number()
    .typeError("Harga Diskon harus berupa angka")
    .required("Harga Diskon Barang harus di isi")
    .test(
      "lessThanHarga",
      "Harga Diskon harus kurang dari Harga Barang",
      function (value) {
        const { harga } = this.parent;
        return value < harga;
      }
    ),
  category: Yup.string().required("Kategori harus di pilih"),
});

const FormUpdate = () => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const { imagePlaceholder, myFetchData, setStatusLoading, statusLoading } =
    useContext(GlobalContext);

  const initialInputData = {
    name: "",
    harga: 0,
    stock: 0,
    description: "",
    image_url: "",
    is_diskon: false,
    harga_diskon: 0,
    category: "",
  };
  const closeModal = () => {
    setModalIsOpen(false);
    navigate("/table");
  };

  const { id } = useParams();
  const navigate = useNavigate();
  const [input, setInput] = useState(initialInputData);

  const myFetchDataId = async () => {
    try {
      let response = await axios.get(
        `https://api-project.amandemy.co.id/api/products/${id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setInput(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setStatusLoading(false);
    }
  };
  const onUpdate = async (values) => {
    try {
      // eslint-disable-next-line
      let putData = await axios.put(
        `https://api-project.amandemy.co.id/api/final/products/${id}`,
        values,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setModalIsOpen(true);
      myFetchData();
      resetForm();
    } catch (error) {
      alert(error.response.data.message);
      console.log(error.response.data.message);
    }
  };

  useEffect(() => {
    myFetchDataId();
    // eslint-disable-next-line
  }, []);
  const {
    handleChange,
    values,
    handleSubmit,
    resetForm,
    touched,
    errors,
    handleBlur,
  } = useFormik({
    initialValues: input,
    onSubmit: onUpdate,
    enableReinitialize: true,
    validationSchema: rulesSchema,
  });
  return (
    <>
      <div className="px-3 md:px-8">
        <section className="max-w-6xl mx-auto shadow-lg rounded-lg bg-white p-8 my-8">
          <h1 className="text-2xl text-cyan-500 text-center">Update Product</h1>
          {statusLoading ? (
            <div className="flex justify-center my-40">
              <span className="loading loading-spinner loading-lg text-cyan-500"></span>
            </div>
          ) : (
            <>
              <div className="flex flex-col md:grid md:grid-cols-5 gap-x-6 gap-y-4 my-2">
                <div className="col-span-3">
                  <label className="block mb-1 md:text-lg text-sm ">
                    Nama Barang
                  </label>
                  <input
                    type="text"
                    name="name"
                    value={values.name}
                    className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
                    placeholder="Masukkan Nama Barang"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
                    {touched.name === true && errors.name}
                  </p>
                </div>
                <div className="col-span-2">
                  <label className="block mb-1 md:text-lg text-sm">
                    Stock Barang
                  </label>
                  <input
                    type="text"
                    name="stock"
                    value={values.stock}
                    className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
                    placeholder="Masukkan Stock Barang"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
                    {touched.stock === true && errors.stock}
                  </p>
                </div>
                <div className="col-span-2">
                  <label className="block mb-1 md:text-lg text-sm">
                    Harga Barang
                  </label>
                  <input
                    type="text"
                    name="harga"
                    value={values.harga}
                    className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
                    placeholder="Masukkan Harga Barang"
                    onBlur={handleBlur}
                    onChange={handleChange}
                  />
                  <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
                    {touched.harga === true && errors.harga}
                  </p>
                </div>
                <div className="flex items-center justify-center mt-4 gap-3">
                  <input
                    type="checkbox"
                    className=""
                    checked={values.is_diskon}
                    name="is_diskon"
                    onChange={handleChange}
                  />

                  <label className="md:text-lg text-sm">Status Diskon</label>
                </div>
                <div className="col-span-2">
                  {values.is_diskon === true && (
                    <>
                      <label className="block mb-1 md:text-lg text-sm">
                        Harga Diskon
                      </label>
                      <input
                        type="text"
                        name="harga_diskon"
                        value={values.harga_diskon}
                        onChange={handleChange}
                        className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
                        placeholder="Masukkan Harga Diskon"
                        onBlur={handleBlur}
                      />
                      <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
                        {values.is_diskon === true &&
                          touched.harga_diskon === true &&
                          errors.harga_diskon}
                      </p>
                    </>
                  )}
                </div>
                <div className="col-span-2">
                  <label className="block mb-1 md:text-lg text-sm">
                    Kategori Barang
                  </label>
                  <select
                    name="category"
                    value={values.category}
                    className="w-full py-1 px-2 rounded-md bg-white border border-gray-400"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    <option value="" disabled>
                      Pilih Kategori
                    </option>
                    <option value="teknologi">Teknologi</option>
                    <option value="makanan">Makanan</option>
                    <option value="minuman">Minuman</option>
                    <option value="hiburan">Hiburan</option>
                    <option value="kendaraan">Kendaraan</option>
                  </select>
                  <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
                    {touched.category === true && errors.category}
                  </p>
                </div>
                <div className="col-span-2">
                  <label className="block mb-1 md:text-lg text-sm flex justify-center">
                    Gambar Barang
                  </label>
                  <div className="flex justify-center my-2">
                    <img
                      src={
                        values.image_url !== ""
                          ? values.image_url
                          : imagePlaceholder
                      }
                      alt="placeholder"
                      className=" h-40 border p-2"
                    />
                  </div>
                  <input
                    type="text"
                    name="image_url"
                    value={values.image_url}
                    className="block border border-gray-400 rounded-md w-full px-2 py-1 text-sm"
                    placeholder="Masukkan Image URL"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
                    {touched.image_url === true && errors.image_url}
                  </p>
                </div>
                <div className="col-span-5">
                  <label className="block mb-1 md:text-lg text-sm">
                    Deskripsi
                  </label>
                  <textarea
                    name="description"
                    rows="6"
                    className="w-full py-1 px-2 rounded-md bg-white border border-gray-400"
                    value={values.description}
                    onChange={handleChange}
                  ></textarea>
                </div>
              </div>
              <div className="flex justify-center p-4 my-4 gap-4 md:justify-end  ">
                <>
                  <button
                    className="btn btn-success px-4 py-1 text-slate-600 text-white"
                    onClick={handleSubmit}
                  >
                    Update
                  </button>
                  <Link to="/table" className="block">
                    <button className="btn btn-info text-white">Back</button>
                  </Link>
                </>
              </div>
            </>
          )}
        </section>
      </div>
      <Modal
        appElement={document.getElementById("root")}
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        className="rounded-lg mx-10 mt-28 h-4/5  "
        style={{
          overlay: {
            backgroundColor: "rgba(0, 0, 0, 0.5)",
          },
        }}
      >
        <div className="text-center md:mx-40 bg-white p-10 rounded-lg">
          <h1 className="text-2xl block my-5">Berhasil Update Produk</h1>

          <div>
            <button
              className="btn btn-info text-white"
              onClick={() => closeModal()}
            >
              Oke
            </button>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default FormUpdate;
