import { Input } from "antd";
import { useFormik } from "formik";
import { Link, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import axios from "axios";
import Modal from "react-modal";
import { useState, useContext } from "react";
import { GlobalContext } from "../context/GlobalContext";

const rulesSchema = Yup.object({
  email: Yup.string().required("Email harus di isi").email("Email tidak valid"),
  password: Yup.string().required("Password harus di isi"),
});

const Login = () => {
  const [pesan, setPesan] = useState({
    sukses: "",
    gagal: "",
  });
  const { modalIsOpen, setModalIsOpen, setStatusLogin } =
    useContext(GlobalContext);

  const closeModal = () => {
    setModalIsOpen(false);
    setPesan({
      sukses: "",
      gagal: "",
    });
  };

  const initialInputData = {
    email: "",
    password: "",
  };

  const navigate = useNavigate();
  const handleOnSubmit = async (values) => {
    try {
      // eslint-disable-next-line

      let response = await axios.post(
        "https://api-project.amandemy.co.id/api/login",
        values
      );
      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("username", response.data.data.user.username);
      navigate("/table");
      setModalIsOpen(true);
      setStatusLogin(true);
      resetForm();
    } catch (error) {
      setPesan({ ...pesan, gagal: error.response.data.info });
      setModalIsOpen(true);
    }
  };

  const { handleChange, values, handleSubmit, errors, touched, resetForm } =
    useFormik({
      initialValues: initialInputData,
      onSubmit: handleOnSubmit,
      validationSchema: rulesSchema,
    });

  return (
    <>
      <div className="bg-white mt-14 rounded-lg py-16 px-12 max-w-xl md:max-w-2xl md:mx-auto mx-10 shadow-lg">
        <h1 className="text-center text-lg md:text-2xl font-bold">
          Login Form
        </h1>
        <div>
          <h1 className="md:text-lg text-sm">Email</h1>
          <Input
            placeholder="Masukan Email"
            style={{ padding: "7px", marginTop: "6px" }}
            name="email"
            onChange={handleChange}
            value={values.email}
            className={
              touched.email === true && errors.email
                ? " border border-red-500"
                : ""
            }
          />
          <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
            {touched.email === true && errors.email}
          </p>
        </div>

        <div className="my-8">
          <h1 className="md:text-lg text-sm">Password</h1>
          <Input.Password
            placeholder="Masukan Password"
            style={{ padding: "7px", marginTop: "6px" }}
            name="password"
            value={values.password}
            onChange={handleChange}
            className={
              touched.password === true && errors.password
                ? "border border-red-500"
                : ""
            }
          />
          <p className="text-red-500 ml-2 font-semibold text-xs md:text-lg">
            {touched.password === true && errors.password}
          </p>
        </div>

        <div className="flex justify-between mt-4">
          <div>
            <h1 className="text-xs md:text-lg">Tidak Punya akun?</h1>
            <Link to="/register" className="inline-block">
              <button className="text-blue-400 hover:text-white hover:text-slate-600 text-xs md:text-lg">
                Register
              </button>
            </Link>
          </div>
          <div>
            <button
              type="button"
              className="btn btn-success btn-sm md:btn-md capitalize text-white"
              onClick={handleSubmit}
            >
              Login
            </button>
          </div>
        </div>
      </div>

      <Modal
        appElement={document.getElementById("root")}
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        className="rounded-lg mx-10 mt-28 h-4/5  "
        style={{
          overlay: {
            backgroundColor: "rgba(0, 0, 0, 0.5)",
          },
        }}
      >
        <div className="text-center md:mx-40 bg-white p-10 rounded-lg">
          {pesan.sukses !== "" && (
            <h1 className="text-2xl block my-5">Berhasil Login</h1>
          )}
          {pesan.gagal !== "" && (
            <h1 className="text-2xl block my-5 text-red-500">{pesan.gagal}</h1>
          )}
          <div>
            <button
              className={
                pesan.gagal !== ""
                  ? "btn btn-error text-white"
                  : "btn btn-info text-white"
              }
              onClick={() => closeModal()}
            >
              Oke
            </button>
          </div>
        </div>
      </Modal>
    </>
  );
};
export default Login;
