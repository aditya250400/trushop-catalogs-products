import logo from "../img/logo.png";
import { Link, useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
const Navbar = () => {
  const location = useLocation();
  const navigate = useNavigate();

  const onLogout = async () => {
    try {
      // eslint-disable-next-line
      let response = await axios.post(
        "https://api-project.amandemy.co.id/api/logout",
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    } catch (error) {
      alert(error.response.data.message);
      console.log(error.response.data.message);
    } finally {
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      navigate("/login");
    }
  };
  return (
    <header className="shadow-lg py-4 bg-white px-12 sticky top-0 z-50">
      <nav className="mx-auto ml-10 md:ml-0 md:px-0 px-28 md:mx-auto w-full md:max-w-7xl flex justify-center md:justify-between">
        <div className="">
          <Link to="/">
            <img src={logo} alt="logo" className="w-0 md:w-32" />
          </Link>
        </div>
        <ul className="flex items-center gap-2 md:gap-6 text-cyan-500 text-xl md:mr-40">
          <li>
            <Link to="/">
              <button
                className={
                  location.pathname === "/"
                    ? "bg-cyan-500 text-white md:p-2 rounded-lg w-14 md:w-20 text-sm p-1 md:text-lg"
                    : "hover:bg-cyan-500 hover:text-white p-1 text-sm hover:md:p-2 hover:rounded-lg md:text-lg"
                }
              >
                Home
              </button>
            </Link>
          </li>
          <li>
            <Link to="/products">
              <button
                className={
                  location.pathname === "/products"
                    ? "bg-cyan-500 text-white md:p-2 rounded-lg w-18 md:20 text-sm p-1 md:text-lg"
                    : "hover:bg-cyan-500 hover:text-white p-1 text-sm hover:md:p-2 hover:rounded-lg md:text-lg"
                }
              >
                Products
              </button>
            </Link>
          </li>
          {localStorage.getItem("token") !== null && (
            <li>
              <Link to="/table">
                <button
                  className={
                    location.pathname === "/table"
                      ? "bg-cyan-500 text-white md:p-2 rounded-lg w-10 md:w-20 text-sm p-1 md:text-lg"
                      : "hover:bg-cyan-500 hover:text-white p-1 text-sm hover:md:p-2 hover:rounded-lg md:text-lg"
                  }
                >
                  Table
                </button>
              </Link>
            </li>
          )}
        </ul>
        <div className="flex items-center ml-14 md:ml-0 md:gap-4 px-6 md:px-0">
          {localStorage.getItem("token") !== null ? (
            <>
              <div className="dropdown dropdown-end">
                <h1
                  className="hover:cursor-pointer text-sm md:text-lg"
                  tabIndex={0}
                >
                  Hai, {localStorage.getItem("username")}
                </h1>
                <ul
                  tabIndex={0}
                  className="mt-3 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box"
                >
                  <li>
                    <button onClick={onLogout}>Logout</button>
                  </li>
                </ul>
              </div>
            </>
          ) : (
            <>
              <Link to="/login">
                <button
                  className={
                    location.pathname === "/login"
                      ? "text-white bg-cyan-500 rounded-lg text-sm md:text-lg p-1"
                      : "text-cyan-500 bg-white border border-cyan-500 rounded-lg p-1 text-xm md:text-lg hover:bg-cyan-500 hover:text-white"
                  }
                >
                  Login
                </button>
              </Link>
            </>
          )}
        </div>
      </nav>
    </header>
  );
};

export default Navbar;
