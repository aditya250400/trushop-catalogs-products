import { createContext, useState } from "react";
import axios from "axios";
import imgPlaceholder from "../img/img-placeholder.png";

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  
  const [products, setProducts] = useState([]);
  const [statusLoading, setStatusLoading] = useState(true);
  const [imagePlaceholder, setImagePlaceholder] = useState(imgPlaceholder);
  const [modalIsOpen, setModalIsOpen] = useState(false); 
  const [statusLogin, setStatusLogin] = useState(false);
  const myFetchData = async () => {
    try {
      let response = await axios.get(
        "https://api-project.amandemy.co.id/api/final/products",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          }
        }
      );

      setProducts(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setStatusLoading(false);
    }
  };

  
  

  return (
    <GlobalContext.Provider
      value={{
        products,
        imagePlaceholder,
        statusLoading,
        modalIsOpen,
        statusLogin,
        setModalIsOpen,
        setStatusLogin,
        setImagePlaceholder,
        setProducts,
        myFetchData,
        setStatusLoading,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
