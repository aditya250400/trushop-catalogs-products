import { GlobalProvider } from "./context/GlobalContext";
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import HomePage from "./pages/HomePage";
import TablePage from "./pages/TablePage";
import UpdatePage from "./pages/UpdatePage";
import CreatePage from "./pages/CreatePage";
import DetailPage from "./pages/DetailPage";
import ProductsPage from "./pages/ProductsPage";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import ProtectedRoute from "./wrapper/ProtectedRoute";
import GuestRoute from "./wrapper/GuestRoute";

function App() {
  return (

    <>
      <BrowserRouter>
        <GlobalProvider>
          <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/products" element={<ProductsPage />} />
            <Route path="/product/:id" element={<DetailPage />} />

            <Route element={<ProtectedRoute />}>
              <Route path="/update/:id" element={<UpdatePage />} />
              <Route path="/create" element={<CreatePage />} />
              <Route path="/table" element={<TablePage />} />
            </Route>

            <Route element={<GuestRoute />}>
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
            </Route>
          </Routes>
        </GlobalProvider>
      </BrowserRouter>

    </>
    )
}

export default App;
