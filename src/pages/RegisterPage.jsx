import { Helmet } from "react-helmet";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import Register from "../components/Register";

const RegisterPage = () => {
  return (
    <>
      <Helmet>
        <title>Register | TruShop Catalogs Products</title>
        <meta content="Register | TruShop Catalogs Products" />
      </Helmet>
      <Navbar />
      <Register />
      <Footer />
    </>
  );
};

export default RegisterPage;
