import Navbar from "../components/Navbar";
import TableProducts from "../components/TableProducts";
import Footer from "../components/Footer";
import { Helmet } from "react-helmet";

const TablePage = () => {
  return (
    <>
      <Helmet>
        <title>Table | TruShop Catalogs Products</title>
        <meta content="Table| TruShop Catalogs Products" />
      </Helmet>
      <Navbar />
      <TableProducts />
      <Footer />
    </>
  );
};

export default TablePage;
