import { Helmet } from "react-helmet";
import Footer from "../components/Footer";
import Login from "../components/Login";
import Navbar from "../components/Navbar";

const LoginPage = () => {
  return (
    <>
      <Helmet>
        <title>Login | TruShop Catalogs Products</title>
        <meta content="Login | TruShop Catalogs Products" />
      </Helmet>
      <Navbar />
      <Login />
      <Footer />
    </>
  );
};

export default LoginPage;
