import Navbar from "../components/Navbar";
import FormCreate from "../components/FormCreate";
import Footer from "../components/Footer";
import { Helmet } from "react-helmet";

const CreatePage = () => {
  return (
    <>
      <Helmet>
        <title>Create Product| TruShop Catalogs Products</title>
        <meta content="Create Product| TruShop Catalogs Products" />
      </Helmet>
      <Navbar />
      <FormCreate />
      <Footer />
    </>
  );
};

export default CreatePage;
