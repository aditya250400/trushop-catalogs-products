import { useContext, useEffect } from "react";
import { GlobalContext } from "../context/GlobalContext";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import { Helmet } from "react-helmet";
import Home from "../components/Home";
import { Link } from "react-router-dom";
import { Carousel } from "antd";

const HomePage = () => {
  const { products, myFetchData, statusLoading } = useContext(GlobalContext);
  const productSlice = products.slice(0, 4);

  useEffect(() => {
    myFetchData();
    // eslint-disable-next-line
  }, []);

  const contentStyle = {
    height: "400px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
  };
  return (
    <>
      <Helmet>
        <title>Home | TruShop Catalogs Products</title>
        <meta content="Home Product| TruShop Catalogs Products" />
      </Helmet>
      <Navbar />
      {statusLoading ? (
        <div className="flex justify-center my-40">
          <span className="loading loading-spinner loading-lg text-cyan-500"></span>
        </div>
      ) : productSlice.length > 0 ? (
        <Carousel autoplay>
          {productSlice.map((data) => {
            return (
              <div key={data.id}>
                <Link to={`/product/${data.id}`}>
                  <img
                    src={data.image_url}
                    className="w-full object-cover"
                    style={contentStyle}
                    alt={data.name}
                  />
                </Link>
              </div>
            );
          })}
        </Carousel>
      ) : (
        <Carousel>
          <div>
            <h1 style={contentStyle} className="text-3xl">
              Belum Ada Produk
            </h1>
          </div>
        </Carousel>
      )}
      <div className="mx-8 md:mx-16 mt-16 flex justify-between">
        <div>
          <h1 className="text-cyan-500 text-lg md:text-3xl">
            Catalog products
          </h1>
        </div>
        <div>
          <Link to="/products">
            <button className="bg-white border border-cyan-500 text-cyan-500 p-1 md:p-2 text-sm rounded-lg hover:bg-cyan-500 hover:text-white">
              See More
            </button>
          </Link>
        </div>
      </div>
      <div className="flex justify-center  p-2 flex-row flex-wrap items-stretch gap-4 items-center p-4 mt-4">
        {statusLoading ? (
          <div className="flex justify-center my-40">
            <span className="loading loading-spinner loading-lg text-cyan-500"></span>
          </div>
        ) : productSlice.length > 0 ? (
          productSlice.map((product, index) => (
            <Home product={product} key={product.id} />
          ))
        ) : (
          <p>Belum ada produk</p>
        )}
      </div>
      <Footer />
    </>
  );
};

export default HomePage;
