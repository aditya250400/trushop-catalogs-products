import Navbar from "../components/Navbar";
import FormUpdate from "../components/FormUpdate";
import Footer from "../components/Footer";
import { Helmet } from "react-helmet";

const UpdatePage = () => {
  return (
    <>
      <Helmet>
        <title>Update Product| TruShop Catalogs Products</title>
        <meta content="Update Product| TruShop Catalogs Products" />
      </Helmet>
      <Navbar />
      <FormUpdate />
      <Footer />
    </>
  );
};

export default UpdatePage;
