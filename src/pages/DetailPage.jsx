import Navbar from "../components/Navbar";
import DetailProduct from "../components/DetailProduct";
import Footer from "../components/Footer";
import { Helmet } from "react-helmet";

const DetailPage = () => {
  return (
    <>
      <Helmet>
        <title>Detail Product | TruShop Catalogs Products</title>
        <meta content="Detail Product | TruShop Catalogs Products" />
      </Helmet>
      <Navbar />
      <DetailProduct />
      <Footer />
    </>
  );
};
export default DetailPage;
